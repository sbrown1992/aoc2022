"""Main module for running aoc"""
import argparse
import cmd


def parse_args():
    """parse_args will parse the arguments passed to the program"""
    arg_parser = argparse.ArgumentParser(
        prog="aoc.py",
        description="Makes running AoC problems ez pz",
    )
    arg_parser.add_argument(
        "-p", "--problem", help="the identifier of the problem (1a, 1b, 2a, 2b, ...)"
    )
    arg_parser.add_argument("-y", "--year", help="the year to work with")
    arg_parser.add_argument(
        "-g", "--get", action="store_true", help="get inputs from aoc site"
    )

    args = arg_parser.parse_args()

    print(f"args={args}")

    day = "1"
    problem = "a"
    year = "2022"
    if args.year:
        year = args.year
    if args.problem:
        day = args.problem[0]
        problem = args.problem[1]

    if args.get:
        cmd.get_input(year, day)

    cmd.run_problem(year, day, problem)


if __name__ == "__main__":
    parse_args()
