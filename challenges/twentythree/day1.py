def readinput(filename):
    with open(filename) as f:
        lines = [line.rstrip() for line in f]
        return lines

def get_first_digit(line):
    for char in line:
        if char.isdigit():
            return int(char)
    return 0

def get_last_digit(line):
    for char in reversed(line):
        if char.isdigit():
            return int(char)
    return 0

def get_line_calibration(line):
    return int(f"{get_first_digit(line)}{get_last_digit(line)}")

def get_alt_first_digit(line):
    if len(line) == 0:
        return 0
    elif line[0].isdigit():
        return int(line[0])
    elif line.startswith("one"):
        return 1
    elif line.startswith("two"):
        return 2
    elif line.startswith("three"):
        return 3
    elif line.startswith("four"):
        return 4
    elif line.startswith("five"):
        return 5
    elif line.startswith("six"):
        return 6
    elif line.startswith("seven"):
        return 7
    elif line.startswith("eight"):
        return 8
    elif line.startswith("nine"):
        return 9
    else:
        return get_alt_first_digit(line[1:])

def get_alt_last_digit(line):
    if len(line) == 0:
        return 0
    elif line[-1].isdigit():
        return int(line[-1])
    elif line.endswith("one"):
        return 1
    elif line.endswith("two"):
        return 2
    elif line.endswith("three"):
        return 3
    elif line.endswith("four"):
        return 4
    elif line.endswith("five"):
        return 5
    elif line.endswith("six"):
        return 6
    elif line.endswith("seven"):
        return 7
    elif line.endswith("eight"):
        return 8
    elif line.endswith("nine"):
        return 9
    else:
        return get_alt_last_digit(line[:-1])

def get_alt_line_calibration(line):
    return int(f"{get_alt_first_digit(line)}{get_alt_last_digit(line)}")

def run1a():
    filename = "input_store/2023/day1.txt"
    total = 0
    for line in readinput(filename):
        total += get_line_calibration(line)

    print(total)

def run1b():
    filename = "input_store/2023/day1.txt"
    total = 0
    for line in readinput(filename):
        total += get_alt_line_calibration(line)
    print(total)

