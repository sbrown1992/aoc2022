from day1 import get_first_digit 
from day1 import get_last_digit
from day1 import get_alt_first_digit
from day1 import get_alt_last_digit

def test_get_first_digit():
    assert get_first_digit("1abc2") == 1
    assert get_first_digit("pqr3stu8vwx") == 3
    assert get_first_digit("a1b2c3d4e5f") == 1
    assert get_first_digit("treb7uchet") == 7

def test_get_last_digit():
    assert get_last_digit("1abc2") == 2
    assert get_last_digit("pqr3stu8vwx") == 8
    assert get_last_digit("a1b2c3d4e5f") == 5
    assert get_last_digit("treb7uchet") == 7

def test_get_alt_first_digit():
    assert get_alt_first_digit("two1nine") == 2
    assert get_alt_first_digit("eightwothree") == 8
    assert get_alt_first_digit("abcone2threexyz") == 1
    assert get_alt_first_digit("xtwone3four") == 2
    assert get_alt_first_digit("4nineeightseven2") == 4
    assert get_alt_first_digit("zoneeight234") == 1
    assert get_alt_first_digit("7pqrstsixteen") == 7

def test_get_alt_last_digit():
    assert get_alt_last_digit("two1nine") == 9
    assert get_alt_last_digit("eightwothree") == 3
    assert get_alt_last_digit("abcone2threexyz") == 3
    assert get_alt_last_digit("xtwone3four") == 4
    assert get_alt_last_digit("4nineeightseven2") == 2
    assert get_alt_last_digit("zoneeight234") == 4
    assert get_alt_last_digit("7pqrstsixteen") == 6