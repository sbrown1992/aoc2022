all:
	make verify

verify:
	python3 -m black *.py
	python3 -m pip freeze > requirements.txt
	python3 -m pylint *.py
	make test

today:
	python3 aoc.py -y 2023 -p 1a -g

test:
	(cd challenges/twentythree && python3 -m pytest)