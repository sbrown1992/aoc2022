"""cmd will run the command(s) specified from the command line"""

import os
from pathlib import Path
import requests
from dotenv import load_dotenv

from challenges.twentythree.day1 import run1a
from challenges.twentythree.day1 import run1b


def run_problem(year, day, problem):
    """runs the specified problem"""
    match year:
        case "2023":
            match day:
                case "1":
                    if problem == "a":
                        run1a()
                    elif problem == "b":
                        run1b()
                    else:
                        print(f"unknown problem {problem}")
                case _:
                    print(f"unknown day {day}")
        case _:
            print(f"unknown year {year}")


def __get_input_from_aoc(session_token, year, day):
    """Gets the input for a given day from the aoc website, using your aoc session cookie token"""
    # cookies = dict(session=session_token)
    cookies = {"session": session_token}
    headers = {"User-Agent": "gitlab.com/sbrown1992/aoc2022 by sbrown1992@gmail.com"}

    r = requests.get(
        f"https://adventofcode.com/{year}/day/{day}/input",
        cookies=cookies,
        headers=headers,
        timeout=5,
    )
    if r.status_code != 200:
        print(f"ERROR: got status code {r.status_code}, expected 200")
        return

    with open(f"input_store/{year}/day{day}.txt", "w+", encoding="utf-8") as f:
        f.write(r.text)


def get_input(year, day):
    """Gets the input for a given day"""
    load_dotenv()
    session_token = os.getenv("AOC_SESSION")

    if not os.path.exists(f"input_store/{year}"):
        Path(f"input_store/{year}").mkdir(parents=True, exist_ok=True)
    if not os.path.exists(f"input_store/{year}/day{day}.txt"):
        print(f"INFO: getting input for day {day}")
        __get_input_from_aoc(session_token, year, day)
