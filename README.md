# aoc

## Setup

1. `cp .env-example .env`
1. Replace the sample session cookie with your own

AoC identifies individual users with a session cookie attached to each request. Replace `AOC_SESSION` in .env with the hex string containing
your session data

Note: Please do not commit your session cookie to another copy of this repo. Also Note: The aoc author would rather you didn't make your input files or puzzle text publically available. See
[here](https://www.reddit.com/r/adventofcode/wiki/faqs/copyright/inputs) and [here](https://www.reddit.com/r/adventofcode/wiki/faqs/copyright/puzzle_texts) for details

## Automation Disclaimer

This repo follows the automation guidelines on the [Advent of Code community wiki](https://www.reddit.com/r/adventofcode/wiki/faqs/automation). Specifically

* Once inputs are downloaded, they are cached locally in `input_store/20XX/dayX.txt` files
* The `User-Agent` header is set to me since I maintain this tool
